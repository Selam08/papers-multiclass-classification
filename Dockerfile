ARG BASE_IMAGE="nvcr.io/nvidia/pytorch:21.07-py3"
FROM ${BASE_IMAGE}

# Uncomment and adapt if code is to be included in the image
#COPY src /work/papers-multiclass-classification/src

# Uncomment and adapt if your R or python packages require extra linux (ubuntu) software
# e.g. the following installs apt-utils and vim; each pkg on its own line, all lines
# except for the last end with backslash '\' to continue the RUN line
#
#USER root
# RUN apt-get update && \
#    apt-get install -y --no-install-recommends \
#    apt-utils \
#    vim

# install the python dependencies
COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

# Jupyter setting
RUN jupyter nbextension enable --py widgetsnbextension
