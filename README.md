# Papers multiclass classification



## Whats in there ?

This repo contains few scripts aimed at classifying scientific papers into journals with corresponding topic.
These scripts are mainly based on fast-bert and huggingface libraries. Therefore, the purpose of this repo is to leverage these tools to allow a fast prototyping of various models for this task.

## Main scripts

- [ ] src/format_utils.py: allows to format articles into sequences. This sequences are then spllited into train, validation and test sets. These 3 datasets contain sequences coming from different articles. 
- [ ] src/train_and_eval.py: launch model training, save it and automatically evaluate performances. Performances plots and reports are recorded into respective models backup location

- [ ] sh/train_exps.sh: allow to train and evaluate several models sequentially


## Project status
Ongoing