#cd /workspace/workdir/

# Produce datasets
python3 src/format_data.py -b t_ratio_07_v_ratio_0125_max_e_len_300_ca_06
python3 src/format_data.py -b t_ratio_07_v_ratio_0125_max_e_len_400_ca_06 -el 400

# Bert experiments
#python3 src/train_and_eval.py -exp bert-small_lr1e-4_6epoch -d t_ratio_07_v_ratio_0125_max_e_len_300_ca_06
python3 src/train_and_eval.py -exp bert-small_lr1e-4_6epoch -ms 400 -d t_ratio_07_v_ratio_0125_max_e_len_400_ca_06

# XLNET experiments
python3 src/train_and_eval.py -exp XLNET_lr1e-4_6epoch -mt xlnet -mn xlnet-base-cased -d t_ratio_07_v_ratio_0125_max_e_len_300_ca_06
python3 src/train_and_eval.py -exp XLNET_lr1e-4_6epoch -mt xlnet -mn xlnet-base-cased -ms 400 -d t_ratio_07_v_ratio_0125_max_e_len_400_ca_06

# RoBERTa experiments lets try...
python3 src/train_and_eval.py -exp roberta_lr1e-4_6epoch -mt roberta -mn roberta-base -d t_ratio_07_v_ratio_0125_max_e_len_300_ca_06
python3 src/train_and_eval.py -exp roberta_lr1e-4_6epoch -mt roberta -mn roberta-base -ms 400 -d t_ratio_07_v_ratio_0125_max_e_len_400_ca_06

