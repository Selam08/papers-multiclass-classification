import pandas as pd
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report
import matplotlib.pyplot as plt
import os
import numpy as np

def plot_journalIn_vs_N_selected(ax, score_per_n_best, score_per_n_best_perc, max_x=None):
    """Produce chart showing the percetage of correct (containing target journal) selected set of journals depending on the number of journal in the set
        Args:
            ax: (matplotlib axis)
            score_per_n_best_perc: (dic) containing number of correct set, with number of journal in set as key
            score_per_n_best_perc: (dic) containing percentages with number of journal in set as key
            max_x: (int) maximum x value to clipp the chart
        Return:
            ax: (matplotlib axis)
            """
    ax.fill_between(x=list(score_per_n_best.keys()), y1=list(score_per_n_best_perc.values()), y2=[100 for k in score_per_n_best.keys()], alpha=0.5, color="r")
    ax.fill_between(x=list(score_per_n_best.keys()), y1=[0 for k in score_per_n_best.keys()], y2=list(score_per_n_best_perc.values()), alpha=0.5)
    ax.set_ylabel("% of papers in the selection")
    ax.set_xlabel("Number of best journal selected")
    ax.xaxis.set_ticks(np.arange(1, max(score_per_n_best.keys())+1, 1))
    ax.grid("on", color="k", alpha=0.2)
    ax.set_ylim(0,100)
    if isinstance(max_x, type(None)):
        ax.set_xlim(1, max(score_per_n_best.keys()))
    else:
        ax.set_xlim(1, max_x)
    return ax

def get_closest_index(target_value, perc_dic):
    """get key corresponding to the value which is the closest to the target
    Args:
        target_value: (float)
        perc_dic: (dic)"""
    dif_dic = {k: abs(target_value - v) for k, v in perc_dic.items()}
    return min(dif_dic, key=dif_dic.get)

def get_report(y_true, y_pred, labels):
    """Generate performance report as pandas DataFrame
        Args:
            y_true: (array_like) true target value
            y_pred: (array_like) predicted value
            labels: (list) class names
        Return:
            class_report: (pandas.DataFrame)"""
    class_report = classification_report(y_true=y_true, y_pred=y_pred, target_names=labels, output_dict=True)
    class_report = pd.DataFrame(class_report).T
    return class_report 

def get_ypred_ytrue(inference, dataset_df, missing_labels=[]):
    """Process inference data and original dataset to compute best recommended class per sequences (y_pred), true class (y_true),
    mappings of class name to numerical class encoding (encod2lab, lab2encod) and result as pandas DataFrame
        Args:
            inference: (List(tuple())) output of predict method from fast-bert models
            dataset_df: (pandas DataFrame) original test dataset, used for inference
            missing_labels: (List) list of label to omit (missing from test dataset)
        Returns:
            y_pred: (pandas Serie)
            y_true: (pandas Serie)
            encod2lab: (dic)
            lab2encod: (dic)
            reslab : (List) list of unique labels which have been infered
            result_df : (pandas DataFrame)
            """
    result_df = pd.DataFrame([dict(t) for t in inference])
    reslab = [l for l in result_df.columns.to_list() if not l in missing_labels]
    encod2lab = {i+1 :l for i, l in enumerate(reslab)}
    lab2encod = {v: k for k, v in encod2lab.items()}
    y_pred = result_df.idxmax(axis=1).apply(lambda x: lab2encod[x])
    y_true = dataset_df.idxmax(axis=1).apply(lambda x: lab2encod[x])
    return y_pred, y_true, encod2lab, lab2encod, reslab, result_df


def plot_confusion_matrix(y_true, y_pred, labels):
    """Generate chart of confusion matrix
        Args:
            y_true: (array_like) true target value
            y_pred: (array_like) predicted value
            labels: (List) list of class names
        Returns:
            fig: (matplotlib.pyplot.figue)
            ax: (matplotlib.pyplot.axes)"""
    fig, ax = plt.subplots(figsize=(25,25))
    cm = confusion_matrix(y_true=y_true, y_pred=y_pred, normalize="true")
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, 
                                  display_labels=labels)
    disp.plot(cmap=plt.cm.Blues, ax=ax, colorbar=False, xticks_rotation="vertical", values_format="0.2f")
    return fig, ax

def benchmarks(y_true, y_pred, test_df, result_df, labels, record_folder):
    """Compute berchmarking metrics (Confusion matrix, Percentage of correct set vs number of class per set, metric report)
        Args:
            y_true: (array_like) true target value
            y_pred: (array_like) predicted value
            test_df: (pandas DataFrame) original test dataset, used for inference
            result_df: (pandas DataFrame) prediction data
            labels: (List) list of class names
            record_folder (str) path location to record charts and report
        Returns:
            None
            """
    # Confusion matrix
    fig_cm, ax_cm = plot_confusion_matrix(y_true, y_pred, labels)
    fig_cm.savefig(os.path.join(record_folder, "confusion_matrix.png"), dpi=200, bbox_inches="tight")

    # Compute usage score
    true_df = test_df.idxmax(axis=1)
    ordering = result_df.apply(lambda row : row.sort_values(ascending=False).keys().to_list(), axis=1)
    assert all(true_df.index==ordering.index)
    score_per_n_best = {n_best: np.sum([true_df[ind] in ordering[ind][:n_best] for ind in range(true_df.shape[0])])
                            for n_best in range(1, 50)}
    score_per_n_best_perc = {k: 100*v/true_df.shape[0] for k, v in score_per_n_best.items()}

    # Plot usage graph
    fig, ax = plt.subplots(2, 1, figsize=(15,10))
    plot_journalIn_vs_N_selected(ax[0], score_per_n_best, score_per_n_best_perc)
    plot_journalIn_vs_N_selected(ax[1], score_per_n_best, score_per_n_best_perc, max_x=15)
    ax[0].set_title("Number of proposal for at least 95%: {0:d}\nNumber of proposal for at least 90%: {1:d}".format(
        get_closest_index(95, score_per_n_best_perc), get_closest_index(90, score_per_n_best_perc)))
    ax[1].set_title("Correct set with 5 best proposal selected: {0:.0f}%\nCorrect set with 3 best proposal selected: {1:.0f}%".format(
        score_per_n_best_perc[5], score_per_n_best_perc[3]))
    ax[0].set_xlabel('')
    fig.savefig(os.path.join(record_folder, "Usage.png"), dpi=200, bbox_inches="tight")

    # compute report
    df_report = get_report(y_true, y_pred, labels)
    df_report.to_csv(os.path.join(record_folder, "report.csv"))
    