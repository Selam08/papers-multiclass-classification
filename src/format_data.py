import os, argparse
import pandas as pd
import numpy as np
from format_utils import *
import more_itertools as mit


def main(origin_data, max_encoder_length, train_ratio, val_ratio, backup_location, casting_amount):
    # load data and filter journal with less than 3 obs (need one per dataset (train, valid, test))
    data_root = os.path.dirname(os.path.abspath(__file__)) + '/../data/'
    origin_data = os.path.join(data_root, "original_data", origin_data)
    df = pd.read_json(origin_data, lines=True)
    df = df.sample(frac=1, random_state=0).reset_index(drop=True)
    df["n_lines"] = df["text"].apply(lambda x: len(x.split("\n")))
    df = df.loc[df.journal.isin([i[0] for i in (df.journal.value_counts()>=3).iteritems() if i[1]])]
    df = df.sample(frac=1, random_state=8).reset_index(drop=True)

    # format paper into obs Sample
    df["formatted_text"] = df.text.apply(lambda x: x.replace("\n", " "))
    df["formatted_text"] = simplify_space(df.formatted_text)
    journal_count = df.journal.value_counts()
    df["low_representation"] = df.journal.isin([i[0] for i in (journal_count<=10).iteritems() if i[1]])
    df["Sample"] = df.formatted_text.apply(lambda x: [" ".join(l) for l in mit.chunked(x.split(" "), max_encoder_length)])

    # label
    labels = ["journal_"+l for l in df.journal.unique()]
    df["group"] = df.journal.copy()
    df = pd.get_dummies(df, columns=["journal"])
    data = df[["Sample", "id", "low_representation", "group"]+labels]

    # split dataset by papers id
    train, val, test = get_stratified_split(data, train_ratio, val_ratio, journal_count.to_dict())
    train, val, test = [d.drop(columns=["group"]) for d in [train, val, test]]

    # split paper by sequence
    train, val, test = [cast_obs_from_sample(d, casting_amount).drop(columns=["low_representation"])
                        for d in [train, val, test]]
    train, val, test = [label_first(d) for d in [train, val, test]]
    train = train.sample(frac=1, random_state=8).reset_index(drop=True)

    # backup
    backup_location = os.path.join(data_root, "formatted_data", backup_location)
    os.makedirs(backup_location, exist_ok=True) 
    train.to_csv(os.path.join(backup_location, "train.csv"), index=False)
    val.to_csv(os.path.join(backup_location, "val.csv"), index=False)
    test.to_csv(os.path.join(backup_location, "test.csv"), index=False)
    pd.DataFrame(labels).to_csv(os.path.join(backup_location, "labels.csv"), index=False, header=False)
    
# main routine
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Format data into trainable sequence')
    parser.add_argument('-d', '--origin_data', help='name of the experience, visible in the resulting csv', 
                        default="Jan2020Frontiers.jsonl", type=str)
    parser.add_argument('-b','--backup_location', help='folder to record formatted data', 
                        default="unique_seq", type=str)
    parser.add_argument('-tr','--train_ratio', help='ratio of data to use for training', default=0.75, type=float)
    parser.add_argument('-vr','--val_ratio', help='ratio of data to use for validation', default=0.125, type=float)
    parser.add_argument('-el','--max_encoder_length', help='max_encoder_length or max sequence length', default=300, type=int)
    parser.add_argument('-ca','--casting_amount', help='amount of sequence frome single papers', default=0.6, type=float)

    
    args = parser.parse_args()
    main(**vars(args))