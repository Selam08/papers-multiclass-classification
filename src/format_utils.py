import pandas as pd

def simplify_space(text_serie):
    """Reduce recursively double spacing of pandas.Series containing text
        Arg:
            text_serie: (pd.Serie)
        return:
            reduced_serie: (pd.Serie)"""
    
    reduced_serie = text_serie.apply(lambda x: x.replace("  ", " "))
    if reduced_serie.apply(lambda x : len(x.split("  "))!=1).sum()==0:
        return reduced_serie
    else:
        return simplify_space(reduced_serie)
    
def cast_obs_from_sample(df, amount=0.6, sample_column="Sample", drop=True, prop_flag="low_representation"):
    """Select first sequences of a Sample list
    Args:
        df: (pd.DataFrame)
        amount: (float, int) ratio or number of sequence to keep
        sample_column: (str) name of the sample colonne
    Returns:
        pd.DataFrame containing observations"""
    if (amount).is_integer():
        if isinstance(prop_flag, str):
            df["text"] = df[sample_column].combine(df[prop_flag], lambda x, y: x if y else x[:int(amount)]).copy()
        else:
            df["text"] = df[sample_column].apply(lambda x: x[:int(amount)]).copy()
    else:
        if isinstance(prop_flag, str):
            df["text"] = df[sample_column].combine(df[prop_flag], lambda x, y: x if y else x[:int(amount*len(x))]).copy()
        else:
            df["text"] = df[sample_column].apply(lambda x: x[:int(amount*len(x))]).copy()
    if drop:
        df.drop(columns=sample_column, inplace=True)
    return df.explode("text").reset_index(drop=True)

def split_seq_number(df, text_column="text"):
    """split sequence and sequence numbering in two columns"""
    df["n_seq"] = df[text_column].apply(lambda x: x[0])
    df[text_column] = df[text_column].apply(lambda x: x[1])
    return df

def label_first(df, id_row="id", drop_id_row=True):
    """"""
    df["initial_sequence"] = (df[id_row].diff()!=0)*1
    if drop_id_row:
        df = df.drop(columns=[id_row])
    return df

def get_stratified_split(df, ratio_tr, ratio_vl, group_count, group_col="group"):
    """Create stratified split of dataframe into train, val and test based on given ratio and"""
    train, val, test = [], [], []
    grp = [g for g in df.groupby("group")]
    for g_name, g_df in grp:
        train_split, val_split = int(group_count[g_name]*ratio_tr), max(1, int(group_count[g_name]*ratio_vl))
        if (train_split+val_split)==group_count[g_name]:
            train_split -=1
        train.append(g_df.iloc[:train_split])
        val.append(g_df.iloc[train_split:train_split+val_split])
        test.append(g_df.iloc[train_split+val_split:])
    return [pd.concat(d) for d in [train, val, test]]
