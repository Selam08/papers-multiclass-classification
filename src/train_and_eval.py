# general stuff
import os, sys, argparse
import pandas as pd
from pathlib import Path
import numpy as np

# base torch
import torch
from transformers import BertTokenizer

# fast-bert stuff
from fast_bert.data_cls import BertDataBunch
from fast_bert.modeling import BertForMultiLabelSequenceClassification
from fast_bert.learner_cls import BertLearner
from fast_bert.metrics import accuracy_multilabel, accuracy_thresh, fbeta, roc_auc

# helpers
from eval_util import *

# logging stuff
import logging
import datetime
torch.cuda.empty_cache()


def main(exp_name, data_name="unique_seq", max_seq_length=300, do_lower_case=True,
         train_batch_size=8, learning_rate=1e-3, num_train_epochs=6, 
         model_type="bert", model_name="bert-base-uncased", fp16=True):
    # fp16=True should allow to reduce vram used (have small gpu)
    # best learning_rate around 2e-2 (lr-find) but lead to exploding gradient > should be reduced
    # Therefore "bert-base-uncase" with learning_rate=1e-3 work only with fp16=False
    
    
    # set up data and model dir
    data_root = os.path.dirname(os.path.abspath(__file__)) + '/../data/'
    model_root = os.path.dirname(os.path.abspath(__file__)) + '/../models/'
    DATA_PATH = Path(os.path.join(data_root, "formatted_data", data_name))
    LABEL_PATH = Path(os.path.join(data_root, "formatted_data", data_name))
    MODEL_PATH=Path(os.path.join(model_root,exp_name))
    LOG_PATH = Path(os.path.join(MODEL_PATH, "logs"))
    OUTPUT_PATH = Path(os.path.join(MODEL_PATH, "output"))
    os.makedirs(LOG_PATH, exist_ok=True)
    
    # set up logger
    run_start_time = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    logfile = str(LOG_PATH/'log-{}-{}.txt'.format(run_start_time, exp_name))
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(name)s -   %(message)s',
        datefmt='%m/%d/%Y %H:%M:%S',
        handlers=[
            logging.FileHandler(logfile),
            logging.StreamHandler(sys.stdout)
        ])
    logger = logging.getLogger()

    # initialize training parameter and log them
    model_config = {"max_seq_length":max_seq_length, 
                    "do_lower_case":do_lower_case,
                    "train_batch_size": train_batch_size,
                    "learning_rate":learning_rate,
                    "num_train_epochs":num_train_epochs,
                    "model_type":model_type,
                    "model_name":model_name,
                    "warmup_proportion": 0.002,
                    "fp16":fp16
                   }
    logger.info(model_config)


    # detect gpu
    device = torch.device('cuda')
    if torch.cuda.device_count() > 1:
        model_config["multi_gpu"] = True
    else:
        model_config["multi_gpu"] = False

    # load labels
    label_cols = pd.read_csv(LABEL_PATH/'labels.csv', header=None).iloc[:,0].to_list()

    # initialize dataset/dataloader
    databunch = BertDataBunch(DATA_PATH, LABEL_PATH, model_config["model_name"], train_file='train.csv', val_file='val.csv',
                              text_col="text", label_col=label_cols, model_type=model_config["model_type"],
                              batch_size_per_gpu=model_config['train_batch_size'], max_seq_length=model_config['max_seq_length'],
                              multi_gpu=model_config["multi_gpu"], multi_label=True)

    # initialize metrics
    metrics = []
    metrics.append({'name': 'accuracy_multilabel', 'function': accuracy_multilabel})
    metrics.append({'name': 'accuracy_thresh', 'function': accuracy_thresh})
    metrics.append({'name': 'roc_auc', 'function': roc_auc})
    metrics.append({'name': 'fbeta', 'function': fbeta})

    # initialize learner
    learner = BertLearner.from_pretrained_model(databunch, model_config["model_name"], metrics=metrics, 
                                                device=device, logger=logger, output_dir=OUTPUT_PATH, 
                                                multi_gpu=model_config["multi_gpu"], is_fp16=model_config["fp16"], 
                                                multi_label=True, logging_steps=0)

    # train
    os.makedirs(OUTPUT_PATH, exist_ok=True)
    learner.fit(model_config["num_train_epochs"], model_config["learning_rate"], validate=True)

    # backup
    learner.save_model()

    ## Start evaluating the performance and record for benchmarks
    # Load and infer test set
    test_df = pd.read_csv(os.path.join(DATA_PATH, "test.csv"))
    missing_labels = [k for k, v in test_df.iloc[:,:-2].sum(axis=0).items() if v==0] # should be []
    test_first_df = test_df.loc[test_df.initial_sequence==1].copy().reset_index(drop=True)

    # format text and apply inference
    test_text, test_first_text = test_df.text.to_list(), test_first_df.text.to_list()
    test_res, test_first_res = learner.predict_batch(test_text, verbose=True), learner.predict_batch(test_first_text, verbose=True)

    # define y_pred, y_true
    y_pred, y_true, encod2lab, lab2encod, reslab, result_df = get_ypred_ytrue(test_res, test_df.iloc[:, :-2], missing_labels)
    y_pred_first, y_true_first, encod2lab_first, lab2encod_first, reslab_first, result_df_first = get_ypred_ytrue(test_first_res, test_first_df.iloc[:, :-2], missing_labels)
    labs, labs_first = [[l[8:] for l in rl] for rl in [reslab, reslab_first]]

    # initialize report folders
    record_folder_all = os.path.join(MODEL_PATH, "reports_all_seq")
    record_folder_first = os.path.join(MODEL_PATH, "reports_initial_seq")

    # Compute report for all sequences
    os.makedirs(record_folder_all, exist_ok=True) 
    benchmarks(y_true.values, y_pred.values, test_df.iloc[:, :-2], result_df, labs, record_folder_all)

    # Compute report for initial sequences only
    os.makedirs(record_folder_first, exist_ok=True) 
    benchmarks(y_true_first.values, y_pred_first.values, test_first_df.iloc[:, :-2], result_df_first, labs_first, record_folder_first)


# main routine
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Format data into trainable sequence')
    parser.add_argument('-exp', '--exp_name', help='name of the experience', 
                        default="bert-best", type=str)
    parser.add_argument('-d','--data_name', help='folder to containing data to use', 
                        default="unique_seq", type=str)
    parser.add_argument('-ms','--max_seq_length', help='maximum length of sequence', default=300, type=int)
    parser.add_argument('-l','--do_lower_case', help='Casting case parameter', default=True, type=bool)
    parser.add_argument('-bs','--train_batch_size', help='batch size for training', default=8, type=int)
    parser.add_argument('-lr','--learning_rate', help='learning rate for training', default=1e-4, type=float)
    parser.add_argument('-ne','--num_train_epochs', help='Number of epoch to train', default=6, type=int)
    parser.add_argument('-mt','--model_type', help='type of model to train', default="bert", type=str)
    parser.add_argument('-mn','--model_name', help='Name of the specific model architecture to train', default="bert-base-uncased", type=str)
    parser.add_argument('-fp','--fp16', help='cast numerical values to float16 for training', default=True, type=bool)
    
    args = parser.parse_args()
    main(**vars(args))
